<?php
/**
 * Created by PhpStorm.
 * User: mdobrenko
 * Date: 5/27/15
 * Time: 2:17 PM
 */

//Connection for the 1.0 database
$con_v1 = mysqli_connect("209.59.134.45", "rsvpify_marekl", "Aquareef545", "rsvpify_app");

//Connection for the 2.0 database
$con_v2 = mysqli_connect("209.59.134.45", "rsvpify_marekl", "Aquareef545", "rsvpify_app_v2");

$email = $_POST['email_value'];
$user_id = "";
$user_first_name = "";
$user_email_address = "";
$user_subscription_id = "";
$user_subscription_paypal_id = "";
$user_subscription_expiration = "";
$user_subdomain = "";
$changeme = "$2y$10$.m4lhdAETRkYY.y2PiVtjuKFaSmnSukZBknyLV4SHfRh.VDo2lzJy";
$changeme_v1 = '$2y$08$vsd/3zb903BK4bses05QO.9lBa2FY4K9RyALpZjjIaqConrytjsm.';
$migration_type = $_POST['taskOption'];

if($migration_type==1)
{
    performMigration($email);
}
else
{
    reverseMigration($email);
}
function reverseMigration($email)
{
    $query = "SELECT * FROM v1_user_migrations
              WHERE v1_user_email = '{$email}'";

    $result = mysqli_query($GLOBALS['con_v2'], $query);
    $row = mysqli_fetch_array($result);


    //Grabs the v2 user ID from the database
    $query_get_id = "SELECT id
    FROM users
    WHERE email = '{$email}'";


    $result_id = mysqli_query($GLOBALS['con_v2'], $query_get_id);
    $v2_user_id = mysqli_fetch_array($result_id);

   if(mysqli_num_rows($result)==0)
   {
       echo "Nobody with that email has been migrated via the migration script. If they exist, they must be migrated manually.";
       echo "<br/>";
   }
    else
    {
        echo "We found a match. Proceeding with migration!";
        echo "<br/>";

        //Removes the user email,sub,and plan type
        $query_update_users = "UPDATE users
        SET email = '',
        stripe_plan = NULL,
        subscription_ends_at = NULL
        WHERE email = '{$email}'
        ";

        mysqli_query($GLOBALS['con_v2'], $query_update_users);

        //Removes the subdomain
        $query_update_events = "UPDATE events
        SET subdomain = ''
        WHERE user_id = {$v2_user_id[0]};
        ";

        echo $query_update_events;
        mysqli_query($GLOBALS['con_v2'], $query_update_events);

        //Grabs the data we need to proceed with the migration before deleting the record
        $GLOBALS['user_id'] = $row[1];
        $GLOBALS['user_email_address'] = $row[2];
        $GLOBALS['user_first_name'] = $row[3];
        $GLOBALS['user_subscription_paypal_id'] = $row[4];
        $GLOBALS['user_subscription_expiration'] = $row[5];
        $GLOBALS['user_subdomain'] = $row[6];

        //Removes the record from the migrations table
        $query_delete = "DELETE FROM v1_user_migrations
        WHERE v1_user_email = '{$email}';
        ";

        //Executes the query
        mysqli_query($GLOBALS['con_v2'], $query_delete);

        restoreV1Fields();
    }
}

function restoreV1Fields()
{

    //Restores user email address field
    $query = "UPDATE users
    SET email = '{$GLOBALS['user_email_address']}',
    password = '{$GLOBALS['changeme_v1']}'
    WHERE id = {$GLOBALS['user_id']}";
    mysqli_query($GLOBALS['con_v1'], $query);


    //Restores the event subdomain
    $query_events = "UPDATE events
    SET subdomain = '{$GLOBALS['user_subdomain']}'
    WHERE user_id = '{$GLOBALS['user_id']}'";
    mysqli_query($GLOBALS['con_v1'], $query_events);

    //Restores the subscription status

    $query_subscriptions = "UPDATE subscriptions
    SET subscription_id = '{$GLOBALS['user_subscription_paypal_id']}'
    WHERE user_id = {$GLOBALS['user_id']}";
    mysqli_query($GLOBALS['con_v1'], $query_subscriptions);

    echo "Migration complete!";
}
//Performs migration for a 1.0 user to a 2.0 user for the specified email address
function performMigration($email)
{
    $results = getUserV1Information($email);
    if(!$results)
    {
        echo "Could not find a user with that email address -- no changes have been made.";
        echo "<br/>";
    }
    else
    {
        echo "User found -- proceeding to remove email address, and gathering relevant user information.";
        echo "<br/>";
        $GLOBALS['user_id'] = $results[0];
        $GLOBALS['user_first_name'] = $results[1];
        $GLOBALS['user_email_address'] = $results[2];

        //Checks to see if the user has a subscription -- if so, we proceed. Otherwise, those lazy goons can just make a new account
        //Free users don't really need any migrations -- tell them to delete their own events.

        if(!doesUserHaveSubscription($GLOBALS['user_id']))
        {
            echo "Problem with the subscription. Aborting.";
            echo "<br />";
        }
        else
        {
            echo "Valid subscription found. Checking to see if the subscription is a Premiere Single Event.";
            echo "<br />";

            //Checks to see if the event type is Premiere Single Event
            if(isPremiereSingleEvent($GLOBALS['user_subscription_id'])==true)
            {
                echo "The subscription is of the type Single Event Premiere. Proceeding with migration.";
                echo "<br/>";

                //Removes the user email address from the record in the v1 users table.
                removeUserEmailV1($results);
                //Nullifies the subscription
                nullifySubscription($GLOBALS['user_subscription_id']);
                //Grabs and assigns the user subdomain to global value user_subdomain
                getUserSubdomain($GLOBALS['user_id']);
                //Nullifies the events associated with the account
                nullifyEvents($GLOBALS['user_id']);

                //Saves all the gathered data for rollback
                saveMigrationData();

                //Performs the migrate
                saveUserDataV2();
            }
            //Returns output indicating migration failed.
            else
            {
                echo "<b> The subscription is not a Premiere Single Event. Notify user that we do not support premiere pro migrations </b>";
                echo "<br/>";
            }
        }
    }
}

//Checks to see if a given subscription is Premiere Single Event. If so, returns true, otherwise false.
function isPremiereSingleEvent($sub_id)
{
    $query = "SELECT plan
    FROM subscriptions
    WHERE id = {$sub_id}";

    $result = mysqli_query($GLOBALS['con_v1'], $query);
    $row = mysqli_fetch_array($result);
    if($row[0]=="single-event")
    {
        return true;
    }
    else
    {
        return false;
    }
}

//Checks to see if the user has a valid subscription in the database.
//Returns true or false
function doesUserHaveSubscription($user_id)
{
    $query = "SELECT * FROM subscriptions WHERE user_id = {$user_id}";

    $result = mysqli_query($GLOBALS['con_v1'], $query);
    $row = mysqli_fetch_array($result);

    if(mysqli_num_rows($result) != 0)
    {
        $GLOBALS['user_subscription_id'] = $row[0];
        $GLOBALS['user_subscription_paypal_id'] = $row[2];
        $GLOBALS['user_subscription_expiration'] = $row[4];

        if($GLOBALS['user_subscription_paypal_id']=='')
        {
            echo "For some reason the subscription_id is null. Check to see if the user's subscription has been processed by PayPal";
            return false;
        }
        else
        {
            return $row[0];
        }
    }
    else
        return false;
}

//Gets the basic information about our 1.0 user: user's id, first name, email address
function getUserV1Information($email)
{
    $query = "SELECT * FROM users WHERE email = '{$email}';";

    $result = mysqli_query($GLOBALS['con_v1'], $query);
    $row = mysqli_fetch_array($result);

    if(mysqli_num_rows($result) != 0)
        return array($row[0],$row[1],$row[2]);
}
//This function will remove the email address associated with a particular user in the DB
//Will also go ahead and store it in the intermediary v2 table in case of rollback
function removeUserEmailV1($user_info_arr)
{
    $query = "UPDATE users
    SET email = ''
    WHERE email = '{$user_info_arr[2]}';";
    $result = mysqli_query($GLOBALS['con_v1'], $query);
}

//Nullifies the subscription_id field for a given sub_id
function nullifySubscription($subscription_id)
{
    $query = "UPDATE subscriptions
    SET subscription_id = ''
    WHERE id = {$subscription_id}";
    $result = mysqli_query($GLOBALS['con_v1'], $query);
    echo "Nullified subscription.";
    echo "<br/>";
}

//Searches the events table, and nullifies records if necessary. Otherwise does nothing.

function getUserSubdomain($user_id)
{
    $query = "SELECT subdomain
    FROM events
    WHERE user_id = {$user_id}";

    $result = mysqli_query($GLOBALS['con_v1'], $query);
    $row = mysqli_fetch_array($result);
    $GLOBALS['user_subdomain'] = $row[0];
}

//Nullifies the events associated with a users subdomain, if any.
function nullifyEvents($user_id)
{
    $query = "UPDATE events
    SET subdomain = ''
    WHERE user_id = {$user_id}";
    $result = mysqli_query($GLOBALS['con_v1'], $query);
    echo "Done nullifying events if there were any. Proceeding to data transfers.";
    echo "<br/>";
}

//Saves the collected data to our 2.0 table in case of migration reversion.
function saveMigrationData()
{
    $query = "INSERT INTO v1_user_migrations
    SET v1_user_id = {$GLOBALS['user_id']},
    v1_user_email = '{$GLOBALS['user_email_address']}',
    first_name = '{$GLOBALS['user_first_name']}',
    subscription_id = '{$GLOBALS['user_subscription_paypal_id']}',
    expiration_date = '{$GLOBALS['user_subscription_expiration']}',
    subdomain = '{$GLOBALS['user_subdomain']}'";


    $result = mysqli_query($GLOBALS['con_v2'], $query);

    echo "Done saving necessary migration rollback information. Proceeding to 2.0 table insertion!";
    echo "<br/>";
}

function saveUserDataV2()
{
    //First lets check if there is a user with that same email address in 2.0. If so, this will update that record

    $query = "SELECT *
    FROM users
    WHERE email = 'mdobrenko1@gmail.com'";

    $result = mysqli_query($GLOBALS['con_v2'], $query);
    $date = date('Y-m-d H:i:s');

    //Case where there is already an entry in the 2.0 database with that email address.
    if(mysqli_num_rows($result) != 0)
    {
        echo "There is already a user with that specified email address. Updating that entry with the information gathered.";
        echo "<br/>";

        $query = "UPDATE users
        SET first_name = '{$GLOBALS['user_first_name']}',
        email = '{$GLOBALS['user_email_address']}',
        password = '{$GLOBALS['changeme']}',
        email_confirmed=1,
        facebook_id=0,
        earnings=0,
        lifetime_earnings=0,
        remember_token='',
        last_login_at='',
        updated_at='{$date}',
        stripe_active=0,
        stripe_id=null,
        stripe_subscription=null,
        subscription_ends_at ='{$GLOBALS['user_subscription_expiration']}',
        stripe_plan='single_event',
        last_four=null,
        admin=0,
        archived=0
        WHERE email = 'mdobrenko1@gmail.com'
        ";
        $result = mysqli_query($GLOBALS['con_v2'], $query);

    }
    //New entry. Simply perform an insert query!
    else
    {
        $query = "INSERT INTO users
        SET first_name = '{$GLOBALS['user_first_name']}',
        email = '{$GLOBALS['user_email_address']}',
        password = '{$GLOBALS['changeme']}',
        email_confirmed=1,
        created_at=,
        facebook_id=0,
        earnings=0,
        lifetime_earnings=0,
        remember_token='',
        last_login_at='',
        updated_at='{$date}',
        created_at='{$date}',
        stripe_active=0,
        stripe_id=null,
        stripe_subscription=null,
        stripe_plan='single_event',
        subscription_ends_at='{$GLOBALS['user_subscription_expiration']}',
        last_four=null,
        admin=0,
        archived=0
        ";

        $result = mysqli_query($GLOBALS['con_v2'], $query);
        echo $query;
        echo "<br/>";
    }

    echo "Migration complete!";
    echo "<br/>";
}

function getUserV2Information($email)
{
    $query = "SELECT * FROM users WHERE email = '{$email}';";

    $result = mysqli_query($GLOBALS['con_v2'], $query);
    $row = mysqli_fetch_array($result);

    if(mysqli_num_rows($result) != 0)
        return array($row[0],$row[1],$row[2]);
}